import numpy as np
import math
import random
import matplotlib.pyplot as plt
import time

#note--for the sake of the animation, I used 20 cars but this works with 1000 cars as well

#setting up the animation

plt.axis([0,100,-2,2])
plt.ion()
plt.show()

  

end = []

for i in range(20):
	end.append(False)

y_pos = []
#only need this list for the animation

for i in range(20):
	y_pos.append(0)

cars_pos = []

for i in range(20):
	cars_pos.append(-i)

#start the cars behind the 0 mile marker

time1 = 0

while all(end) != True:
	deer = random.randrange(100)
	prob = random.random()
	#random probability of a deer jumping at random mile marker


	if prob < 0.10:
		j = 0
		while j < 3:
			for i in range(len(cars_pos)):
				if (cars_pos[i] <= deer) and (deer - cars_pos[i] < 2.0):
					for k in range(len(cars_pos)):
						if cars_pos[k] <= deer:
							cars_pos[k] = cars_pos[k] + 0.2
						if cars_pos[k] >= 100:
							cars_pos[k] = 100
						if cars_pos[k] == 100:
							end[i] = True 
				elif cars_pos[i] >= deer:
					cars_pos[i] = cars_pos[i] + 1
					if cars_pos[i] >= 100:
						cars_pos[i] = 100
					if cars_pos[i] == 100:
						end[i] = True 

				j += 1
				time1 += 1
				plt.clf()
				plt.xlim(0,100)
				plt.scatter(cars_pos,y_pos, s=50, c='r', marker='D')
				plt.draw()
				


	else:
		for i in range(len(cars_pos)):
			cars_pos[i] = cars_pos[i] + 1
			if cars_pos[i] >= 100:
				cars_pos[i] = 100
			if cars_pos[i] == 100:
				end[i] = True 
	#if the cars reach the end, then flip their index to 'True'
		time1 += 1 
		plt.clf()
		plt.xlim(0,100)
		plt.scatter(cars_pos,y_pos, s=50, c='r', marker='D')
		plt.draw()
		
	

	

	#plot stuff



print time1



